# CMPT 120
# Matthew A Johnson
# 30 Oct 2016
###

MAP = '''
          Waterfall --- Cave --- Tunnel
              |                    |
              |                    |
    Hut --- Jungle                 |
              |                    |
              |                    |
            Beach  ---- Rocks      |
                          |        |
                          |        |
                     Underwater ---+
'''

locNames = [
        "Beach", "Jungle", "Hut", "Waterfall",
        "Cave", "Rocks", "Tunnel", "Underwater"
        ]
locales = [
        "You find yourself on an empty beach. "
        "You see various debris strewn along the shoreline. "
        "The jungle looms a short distance away.",
        "You walk into the dark jungle, where the air is damp and thick. "
        "Unfamiliar noises only add to you feeling of unease. "
        "You can make out a faint trail winding through the trees.",
        "The path leads to a small, decrepit thatch hut."
        "The entrance to the structure is clear; what door there might"
        "once have been is long gone.",
        "A short distance beyond the hut towers an impressive cataract. "
        "The rushing of the water drowns out all other sounds, "
        "and you feel immediately at peace.",
        "You squeeze between the wet rock behind the waterfall and "
        "discover a large cave.",
        "At one end, the beach rises up and gives way to a rocky "
        "outcropping that drops off suddenly to the dark sea below.",
        "Below the waves on the surface lies an opening in the rocks "
        "that leads to an underwater tunnel.",
        "Scrambling down a slope at the back of the cave, you enter a "
        "narrow, descending tunnel that plunges into dark water below."
        ]
visited = [ False, False, False, False, False, False, False, False ]

title = ("Stranded\n"
         "========\n")

intro = ("Over the past several years, you have been training for this moment."
         " Countless hours studying the maps and practicing off the coast "
         "whenever the seas were less than placid to prepare yourself for the "
         "worst. Now, the moment has arrived, and you embark of this voyage "
         "full of anticipation. Sailing into one of the last unexplored "
         "regions of the Pacific Ocean, you feel certain that you will find "
         "something no one else has seen, and the history books will list "
         "your name among the great explorers of the modern age. On the third "
         "night of your journey, a torrential rain begins to fall and the "
         "winds howl and swirls around you and you vessel. You draw upon all "
         "your training and preparation, but to no avail... your boat "
         "capsizes and dump you fericiously into the dark, raging waters. "
         "Suddenly, blackness envelopes you and an icy chill spreads "
         "throughout your body...\n")

ending = ("Suddenly, the crack of a twig behind you is the only warning you "
          "have before being mauled and eaten by a hungry beast. Game over!")

copyright = "\nCopyright (c) 2016 Matthew A Johnson, matthew.johnson1@marist.edu"

MAX_MOVES = 20

def showIntro():
    print(title)
    print(intro)
    input("Press enter to continue...\n")

def showOutro():
    print(ending)
    print(copyright)

def setupPlayer():
    global score, currentLoc, name, moves
    score = 0
    moves = 0
    name = input("You awake slowly, head throbbing. What is your name? ")
    print("Yes, " + name + "... that's your name. Now if only you had "
            "any idea where you are...")
    goBeach()

def getCommand():
    cmd = input("\nWhat do you do next? ").strip().lower()
    return cmd

def renderGame():
    print("\nLocale:", locNames[currentLoc], "    Score:", score, "    Moves:", moves)
    print(locales[currentLoc])

def doGameLoop():
    while True:
        renderGame()

        cmd = getCommand()

        if cmd == "quit" or moves > MAX_MOVES:
            break
        elif cmd == "help":
            print("Valid commands are the cardinal directions, 'help', and 'quit'.")
        elif cmd == "map":
            print(MAP)
        elif cmd == "north":
            if currentLoc == 0: # beach
                goJungle()
            elif currentLoc == 1: # jungle
                goWaterfall()
            elif currentLoc == 7: # underwater
                goRocks()
            else:
                print("You cannot go north from here.")
        elif cmd == "south":
            if currentLoc == 1: # jungle
                goBeach()
            elif currentLoc == 3: # waterfall
                goJungle()
            elif currentLoc == 5: # rocks
                goUnderwater()
            elif currentLoc == 6: # tunnel
                goUnderwater()
            else:
                print("You cannot go south from here.")
        elif cmd == "east":
            if currentLoc == 0: # beach
                goRocks()
            elif currentLoc == 2: # hut
                goJungle()
            elif currentLoc == 3: # waterfall
                goCave()
            elif currentLoc == 4: # cave
                goTunnel()
            elif currentLoc == 7: # underwater
                goTunnel()
            else:
                print("You cannot go east from here.")
        elif cmd == "west":
            if currentLoc == 1: # jungle
                goHut()
            elif currentLoc == 4: # cave
                goWaterfall()
            elif currentLoc == 5: # rocks
                goBeach()
            elif currentLoc == 6: # tunnel
                goCave()
            else:
                print("You cannot go west from here.")
        else:
            print("Invalid command!\n")

def main():
    showIntro()
    setupPlayer()
    doGameLoop()
    showOutro()

def goBeach():
    global currentLoc, score, moves
    currentLoc = 0
    moves += 1
    if not visited[currentLoc]:
        score += 5
        visited[currentLoc] = True

def goJungle():
    global currentLoc, score, moves
    currentLoc = 1
    moves += 1
    if not visited[currentLoc]:
        score += 5
        visited[currentLoc] = True

def goHut():
    global currentLoc, score, moves
    currentLoc = 2
    moves += 1
    if not visited[currentLoc]:
        score += 10
        visited[currentLoc] = True

def goWaterfall():
    global currentLoc, score, moves
    currentLoc = 3
    moves += 1
    if not visited[currentLoc]:
        score += 5
        visited[currentLoc] = True

def goCave():
    global currentLoc, score, moves
    currentLoc = 4
    moves += 1
    if not visited[currentLoc]:
        score += 10
        visited[currentLoc] = True

def goRocks():
    global currentLoc, score, moves
    currentLoc = 5
    moves += 1
    if not visited[currentLoc]:
        score += 5
        visited[currentLoc] = True

def goTunnel():
    global currentLoc, score, moves
    currentLoc = 6
    moves += 1
    if not visited[currentLoc]:
        score += 10
        visited[currentLoc] = True

def goUnderwater():
    global currentLoc, score, moves
    currentLoc = 7
    moves += 1
    if not visited[currentLoc]:
        score += 10
        visited[currentLoc] = True

main()

